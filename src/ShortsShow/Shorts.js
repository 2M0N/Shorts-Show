import React from 'react'
import '../ShortsShow/Shorts.css'
import Data from './Data'
import { Videos } from './Videos'

export const Shorts = () => {

    const date = Data;
    return (
       <div className='all'> 
       <div className='main'>
            <div className='head'> Videola for you ...  </div>

        </div>

        <div className='video_hold'> 
        {date.map((e) => {
            return <Videos key={e.id} url={e.url} />
        })}
        </div>
        </div>
    )
}
